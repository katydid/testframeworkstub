
Feature: SpecFlowFeature1


@tc:7
@System.Component.Feature.Function.1
Scenario: Steps are traced properly
    Given test is started

@tc:8
@System.Component.Feature.Function.2
@Capability.1
Scenario: Test has failed
    Given test is started
     Then test result is Passed

@tc:9
@System.Component.Feature.Function.3
Scenario Outline: Test result is correct
    Given test is started
     Then test result is <Result>

    @Capability.2
    Examples:
    | Result |
    | Passed |

    @Capability.3
    Examples:
    | Result  |
    | Failed  |
