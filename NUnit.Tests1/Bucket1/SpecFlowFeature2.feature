
Feature: SpecFlowFeature2


@tc:6
@System.Component.Feature.Function.3
Scenario Outline: Test result is correct
    Given test is started
     Then test result is <Result>

    @Capability.1
    @Capability.4
    Examples:
    | Result |
    | Passed |

    @Capability.2
    Examples:
    | Result |
    | Failed |

    @Capability.3
    Examples:
    | Result |
    | Passed |
