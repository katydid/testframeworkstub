﻿using System;
using System.IO;
using System.Collections.Generic;
using NUnit.Framework;

namespace TestFramework
{
    class Configuration
    {
        static private readonly string fileName = @".\TestConfiguration.ini";
        static private Dictionary<string, bool> capabilities = new Dictionary<string, bool>();

        static Configuration()
        {
            if (!File.Exists(fileName))
            {
                Assert.Fail("Failed to find configuration file {0}", fileName);
            }

            using (StreamReader streamReader = new StreamReader(fileName))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    string[] pair = line.Split('=');
                    bool value = false;
                    if (pair.Length == 2 && Boolean.TryParse(pair[1], out value))
                    {
                        capabilities.Add(pair[0], value);
                    }
                }
            }
        }

        public static bool CapabilityKnown(string name) => capabilities.ContainsKey(name);
        public static bool CapabilityActive(string name) => CapabilityKnown(name) ? capabilities[name] : false;
    }
}
