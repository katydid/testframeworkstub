﻿using TechTalk.SpecFlow;
using System.Collections.Generic;
using NUnit.Framework;


namespace TestFramework
{
    public enum TestResult
    {
        Passed,
        Failed,
        Inconclusive,
        Ignored
    };

    [Binding]
    public static class TestFramework
    {
        [BeforeTestRun]
        public static void BeforeTestRun()
        {
        }

        [BeforeScenario]
        public static void BeforeScenario()
        {
            analyzeTags();
        }

        private static void analyzeTags()
        {
            var tags = new List<string>();
            var featureTags = FeatureContext.Current.FeatureInfo.Tags;
            var scenarioTags = ScenarioContext.Current.ScenarioInfo.Tags;
            tags.AddRange(featureTags);
            tags.AddRange(scenarioTags);
            foreach (var tag in tags)
            {
                if (Configuration.CapabilityKnown(tag) && !Configuration.CapabilityActive(tag))
                {
                    Assert.Inconclusive("Test is not applicable for running configuration");
                }
            }
        }
    }
}
