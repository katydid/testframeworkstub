﻿using TechTalk.SpecFlow;
using NUnit.Framework;

namespace TestFramework
{
    [Binding]
    public class StepDefinitions
    {
        [Given(@"test is started")]
        public void GivenTestIsStarted()
        {
        }

        [Then(@"test result is (.*)")]
        public void ThenTestResultIs(TestResult result)
        {
            switch (result)
            {
                case TestResult.Passed:
                    Assert.Pass();
                    break;
                case TestResult.Failed:
                    Assert.Fail();
                    break;
                case TestResult.Inconclusive:
                    Assert.Inconclusive();
                    break;
                case TestResult.Ignored:
                    Assert.Ignore();
                    break;
            }
        }
    }
}
