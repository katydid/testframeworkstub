﻿using System;
using TechTalk.SpecFlow.Tracing;

namespace TestFramework
{
    class LogTracer : ITraceListener
    {
        public void WriteTestOutput(string message)
        {
            Console.WriteLine("[{0}] {1}", DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), message);
        }

        public void WriteToolOutput(string message)
        {
        }
    }
}
