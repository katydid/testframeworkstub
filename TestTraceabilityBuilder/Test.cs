﻿
namespace TestTraceabilityBuilder
{
    enum TestResult
    {
        None,
        Passed,
        Failed,
        Ignored,
        Inconclusive
    }

    class Test
    {
        #region public
        public string Id { get; set; }
        public string Name { get; set; }
        public TestResult Result { get; set; }
        public string Color { get; set; }

        public Test(string id, string name, string result)
        {
            Id = id;
            Name = name;
            setResult(result);
        }

        public static string GetColor(TestResult result)
        {
            string color = "#FFFFFF";
            switch (result)
            {
                case TestResult.Passed:
                    color = "#22B14C";
                    break;
                case TestResult.Failed:
                    color = "#ED1C24";
                    break;
                case TestResult.Ignored:
                    color = "#FFF200";
                    break;
                case TestResult.Inconclusive:
                    color = "#00A2E8";
                    break;
            }

            return color;
        }
        #endregion

        #region private
        private void setResult(string name)
        {
            Result = TestResult.None;
            switch (name)
            {
                case "Passed":
                    Result = TestResult.Passed;
                    break;
                case "Failed":
                    Result = TestResult.Failed;
                    break;
                case "Skipped":
                    Result = TestResult.Ignored;
                    break;
                case "Inconclusive":
                    Result = TestResult.Inconclusive;
                    break;
            }
            Color = GetColor(Result);
        }
        #endregion
    }
}
