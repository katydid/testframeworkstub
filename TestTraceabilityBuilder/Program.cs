using System.Linq;
using System.Xml.Linq;

namespace TestTraceabilityBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            string defaultXml = @".\TestResult.xml";
            string defaultHtml = @".\TestTraceabilityMatrix.html";
            string xmlFileName = args.Count() > 0 ? args[0] : defaultXml;
            string htmlFileName = args.Count() > 1 ? args[1] : defaultHtml;

            TraceMatrix tm = new TraceMatrix(xmlFileName);

            XDocument result = new XDocument(
                new XElement("html",
                    new XElement("head"),
                        new XElement("body",
                            new XElement("h1", "Test Traceability Matrix"),
                            tm.GetHtmlCompact(CategoryType.Function),
                            tm.GetHtmlCompact(CategoryType.Capability),
                            tm.GetHtmlDetailed(CategoryType.Function),
                            tm.GetHtmlDetailed(CategoryType.Capability),
                            new XElement("h2", "Agenda"),
                            tm.GetAgenda()
                        )
                    )
                );

            result.Save(htmlFileName);
        }
    }
}
