﻿using System;

namespace TestTraceabilityBuilder
{
    enum CategoryType
    {
        None,
        Function,
        Capability
    }

    class Category : IEquatable<Category>
    {
        #region public
        public string Name { get; set; }
        public CategoryType Type { get; set; }

        public Category(string name)
        {
            Name = name;
            Type = getTypeByName(Name);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Category);
        }

        public bool Equals(Category obj)
        {
            return obj != null && obj.Name.ToLower().Equals(this.Name.ToLower());
        }
        #endregion

        #region private
        private CategoryType getTypeByName(string name)
        {
            return
                name.StartsWith("System")
                    ? CategoryType.Function
                    : name.StartsWith("Capability") ? CategoryType.Capability : CategoryType.None;
        }
        #endregion
    }
}
