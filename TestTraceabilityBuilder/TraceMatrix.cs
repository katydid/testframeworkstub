﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;

namespace TestTraceabilityBuilder
{
    class TraceMatrix
    {
        #region public
        public TraceMatrix(string filename, int version = 3)
        {
            switch (version)
            {
                case 3:
                    tests = (
                        from t in XDocument.Load(filename).Descendants("test-case")
                        select new Test(t.Attribute("id").Value, t.Attribute("fullname").Value, t.Attribute("result").Value)
                    ).ToList();

                    List<Category> categories = (
                        from p in XDocument.Load(filename).Descendants("property")
                        where p.Attribute("name").Value.Equals("Category")
                        orderby p.Attribute("value").Value ascending
                        group p by p.Attribute("value").Value into c
                        select new Category(c.FirstOrDefault().Attribute("value").Value)
                    ).ToList();

                    foreach (Category category in categories)
                    {
                        List<Test> results = (
                            from t in XDocument.Load(filename).Descendants("test-case")
                            where t.AncestorsAndSelf().Elements("properties").Elements("property").Where(
                                x => x.Attribute("name").Value.Equals("Category") &&
                                x.Attribute("value").Value.Equals(category.Name)).Count() > 0
                            select new Test(t.Attribute("id").Value, t.Attribute("fullname").Value, t.Attribute("result").Value)
                        ).ToList();

                        matrix.Add(category, results);
                    }

                    break;
                case 2:
                    // todo: support NUnit2 XML schema
                    break;
                default:
                    break;
            }
        }

        public XElement GetHtmlCompact(CategoryType filter = CategoryType.Function)
        {
            return
                new XElement("table", new XAttribute("border", 1),
                    new XElement("thead",
                        new XElement("h2", filter + " Compact View"),
                        new XElement("tr",
                            new XElement("th", ""),
                            new XElement("th", "Result")
                        )
                    ),
                    new XElement("tbody",
                        from c in matrix.Keys
                        where c.Type.Equals(filter) || filter.Equals(CategoryType.None)
                        select new XElement("tr",
                            new XElement("td", new XElement("b", c.Name)),
                            new XElement("td",
                                new XAttribute("bgcolor", Test.GetColor(getResult(c))))
                        )
                    )
                );
        }

        public XElement GetHtmlDetailed(CategoryType filter = CategoryType.Function)
        {
            return
                new XElement("table", new XAttribute("border", 1),
                    new XElement("thead",
                        new XElement("h2", filter + " Detailed View"),
                        new XElement("tr",
                            new XElement("th", ""),
                            from t in tests
                            select new XElement("th", t.Id, new XAttribute("title", t.Name))
                        )
                    ),
                    new XElement("tbody",
                        from c in matrix.Keys
                        where c.Type.Equals(filter) || filter.Equals(CategoryType.None)
                        select new XElement("tr",
                            new XElement("td", new XElement("b", c.Name)),
                            from t in tests
                            select new XElement("td",
                                new XAttribute("bgcolor", matrix[c].Find(x => x.Id == t.Id) != null ? t.Color : ""))
                        )
                    )
                );
        }

        public XElement GetAgenda()
        {
            return
                new XElement("table", new XAttribute("border", 1),
                    from r in (TestResult[])Enum.GetValues(typeof(TestResult))
                    select new XElement("tr",
                        new XElement("td", r, new XAttribute("bgcolor", Test.GetColor(r)))
                    )
                );
        }
        #endregion

        #region private
        private readonly Dictionary<Category, List<Test>> matrix = new Dictionary<Category, List<Test>>();
        private readonly List<Test> tests = new List<Test>();

        private TestResult getResult(Category category)
        {
            if (!matrix.ContainsKey(category))
            {
                return TestResult.None;
            }

            TestResult result = TestResult.Passed;
            if (matrix[category].Any(x => x.Result == TestResult.Failed))
            {
                result = TestResult.Failed;
            }
            else if (matrix[category].Any(x => x.Result == TestResult.Ignored))
            {
                result = TestResult.Ignored;
            }
            else if (matrix[category].All(x => x.Result == TestResult.Inconclusive))
            {
                result = TestResult.Inconclusive;
            }

            return result;
        }
        #endregion
    }
}
